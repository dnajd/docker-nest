# https://www.gnu.org/software/make/manual/make.html#Introduction
MAKE_FILE := $(lastword $(MAKEFILE_LIST))

# setup

init:
	@docker-compose build
	@docker-compose run --rm web nest new .
	@docker-compose run --rm web npm install 
	@$(MAKE) -f $(MAKE_FILE) own_it

own_it:
	@sudo chown -R $$(whoami):$$(whoami) .

clean:
	@$(MAKE) -f $(MAKE_FILE) own_it
	git clean -fd
	rm -rf node_modules dist
	git checkout .

# daily work

up:
	docker-compose up -d

build:
	docker-compose run --rm npm install 
	docker-compose restart web

down:
	docker-compose down --volumes

bash:
	docker-compose run --rm web bash

unit_test:
	docker-compose run --rm web npm run test:watch

revert:
	git clean -fd
	git checkout .
