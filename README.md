# Nest

Turnkey. Tidy. Nest Js in docker.

# Get Started

Look at the Makefile to see simple targets to get you started

# Explaination

It would be great if the dev community would make things Tidy. If things reliably stood up with a single command. 

What does all this mean?

You can browse you brand new, fully functional, nest js application that just works

```
make init
make up
```

You can take it down.

```
make down
```

You can build your node packages any time you want

```
make build
```

You can run nest cli commands and it works.

```
bin/ncmd nest <your_command_here>
```
