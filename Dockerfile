FROM node:16

RUN mkdir -p /app
ADD . /app
WORKDIR /app

RUN npm i -g @nestjs/cli

CMD ["npm","start"]